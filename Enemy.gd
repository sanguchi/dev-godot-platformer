extends KinematicBody2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var motion = Vector2.ZERO
var direction = 1
const TARGET_FPS = 60
const ACCELERATION = 14
const MAX_SPEED = 20
const FRICTION = 16
const AIR_RESISTANCE = 4
const GRAVITY = 8
const JUMP_FORCE = 140

# Called when the node enters the scene tree for the first time.
func _ready():
	motion.x = -direction


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	motion.x += direction * ACCELERATION * delta * TARGET_FPS
	motion.x = clamp(motion.x, -MAX_SPEED, MAX_SPEED)
	$Sprite.flip_h = motion.x > 0
	motion = move_and_slide(motion, Vector2.UP, false, 4, PI/4, false)
	motion.y = 0
	if(get_slide_count()):  # Wall collision
		# print(get_slide_collision(0).collider.name)
		if(get_slide_collision(0).collider.name in ["TileMap", "StaticBody2D", "Box"]):
			self.direction = -self.direction
		for i in range(get_slide_count()):
			if(get_slide_collision(i).collider.name == "Player"):
				LevelLoader.load_sector("res://Sectors/FirstSector.tscn")

			




