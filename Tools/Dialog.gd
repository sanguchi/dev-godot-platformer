extends Control


export (Array, String) var dialog = []
var dialog_index = 0

# Is the dialog finished printing?
var finished = false
# How much time takes to display a full message (64 chars)
# This determines speed of dialog, less is faster
export (float) var DIALOG_TIME = 4.0

# Emmited before exit
signal dialog_finished

# Called when the node enters the scene tree for the first time.
func _ready():
	assert(dialog.size() > 0, "ERROR, dialog array can't be empty")
	process_dialog()
		

# 16 characters x 4 lines = 64 letters in one second to display (68 chars max)
# this is a speed of (1 second / 64 chars = 0.015 seconds) chars per second 


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	$TextureRect/Label.visible = finished
	if Input.is_action_just_pressed("controls_fire") or Input.is_action_just_pressed("controls_jump"):
		if(finished):
			process_dialog()
		else:
			$Tween.seek(DIALOG_TIME)
			finished = true


func process_dialog():
	if dialog_index < dialog.size():
		finished = false
		var next_dialog: String = dialog[dialog_index]
		var target_time = (DIALOG_TIME / 64) * next_dialog.length()
		# print("Loading text: ", dialog[dialog_index], " ", next_dialog.length())
		# print("Target Time = ", target_time, " seconds at ", DIALOG_TIME / 64, " chars per second")
		$TextureRect/RichTextLabel.bbcode_text = next_dialog
		$TextureRect/RichTextLabel.percent_visible = 0
		$Tween.interpolate_property($TextureRect/RichTextLabel, "percent_visible", 0, 1, target_time, Tween.TRANS_LINEAR, Tween.EASE_IN_OUT)
		$Tween.start()
		dialog_index += 1
	else:
		emit_signal("dialog_finished")
		queue_free()
		# self.visible = false



func _on_Tween_tween_completed(_object, _key):
	finished = true
