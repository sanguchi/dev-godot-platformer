extends Node2D


signal toggle_button(pressed)
var pressed = false

# Called when the node enters the scene tree for the first time.
func _ready():
	pressed = false


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	$Sprite.frame = pressed






func _on_Area2D_body_entered(_body):
	# print("[Button]: ", body)
	pressed = 1
	emit_signal("toggle_button", pressed)


func _on_Area2D_body_exited(_body):
	# print("[Button]: ", body)
	pressed = 0
	emit_signal("toggle_button", pressed)
