extends Node2D
# True opens door
func _on_signal_received(open: bool):
	if(open):
		$AnimationPlayer.play("open")
	else:
		$AnimationPlayer.play("close")


