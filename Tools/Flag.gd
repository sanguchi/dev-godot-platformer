extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var time = 0
var first_time = true
# Called when the node enters the scene tree for the first time.
func _ready():
	time = 0

func _process(delta):
	time = time + delta

func _on_Area2DAction_area_entered(_area):
	# print("AREA ENTERED ", area)
	$ActionHint.visible = true
	# print(area.get_parent())


func _on_Area2DAction_area_exited(_area):
	$ActionHint.visible = false
	# print("AREA EXITED ", area)


func _on_ActionHint_action():
	# print("Action signal received")
	if(first_time):
		LevelLoader.load_dialog(["FINISHED IN %.2f SECONDS" % time])
		first_time = false
	else:
		LevelLoader.load_dialog(["PLEASE MOVE ON, I ALREADY TOLD YOU THE SPEEDRUN TIME", "???", "-.-\""])

	
