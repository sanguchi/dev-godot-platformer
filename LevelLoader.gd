extends Node2D

# Game is a giant metroidvania world
# is divided in sectors
# each sector has many rooms
# Room loader loads an entire sector then:
# - Moves camera across rooms
# - Manages Dialog engine
# - Spawns Player on sector
# - idk xd

onready var Dialog: Control = $Dialog
onready var Player: KinematicBody2D = $Player
onready var Inventory: Node2D = $CanvasLayer/player_inventory

var current_level: Node2D = null
var camera_position = Vector2()
# Called when the node enters the scene tree for the first time.
func _ready():
	# Load Sector  TODO: save game (last sector and last room)
	if(current_level == null):
		current_level = get_tree().current_scene
	if(Dialog != null):
		Dialog.queue_free()
	# Setup Camera management
	var notifier: VisibilityNotifier2D = Player.get_node("VisibilityNotifier2D")
	var _connect_id = notifier.connect("viewport_exited", self, "_on_VisibilityNotifier2D_viewport_exited")
	get_tree().paused = true
	# print("Current Scene: ", get_tree().current_scene.name)
	# print("Application Config Starting Scene: ", ProjectSettings.get_setting('application/run/main_scene'))
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


# Label para empezar
# Funcion para poner al player
# Inicializar Dialog
# Guardar ultima escena

func load_sector(sector_name: String):
	# Load scene and Player
	current_level.queue_free()
	current_level = load(sector_name).instance()
	# Player = load("res://Entities/Player.tscn").instance()
	add_child(current_level)
	# add player to sector
	var starting_pos: Position2D = current_level.get_node("PlayerStartingPoint")
	Player.position = starting_pos.position
	# Player.
	# current_level.add_child(Player)
	# get_tree().change_scene(sector_name)
	get_tree().paused = false

func load_dialog(dialog_text: Array):
	# print("[LevelLoader]: Loading Dialog")

	Dialog = load("res://Tools/Dialog.tscn").instance()
	Dialog.set("dialog", dialog_text)
	# Dialog.instance()
	add_child(Dialog)
	Dialog.show_on_top = true
	var _error = Dialog.connect("dialog_finished", self, "_on_Dialog_finished")
	get_tree().paused = true
	
func _on_Dialog_finished():
	get_tree().paused = false




# TODO: CAMERA ROOM MOVEMENT
func _on_VisibilityNotifier2D_viewport_exited(viewport):
	# print("Player out of room ", Player.position, Player.get_node("VisibilityNotifier2D").position)
	
#	var fliped: bool = Player.get_node("Sprite").flip_h
#	if fliped:
#		Player.position.x -= 8
#	else:
#		Player.position.x += 8
	var player_pos = Player.position + Player.get_node("VisibilityNotifier2D").position
	# If flip true, player is facing right
	
	# TODO: FIX VERTICAL ROOM MOVEMENT

	var x = floor(player_pos.x / (viewport.size.x))
	var y = floor(player_pos.y / (viewport.size.y))
	camera_position = Vector2(x, y) * Vector2(viewport.size)
	# print(grid_position)
	$AnimationPlayer.play("RoomTransitionStart")
	get_tree().paused = true
	yield($AnimationPlayer, "animation_finished")
	$Camera2D.position = camera_position
	$AnimationPlayer.play("RoomTransitionFinish")
	get_tree().paused = false

# MORE SIGNS
# DOOR SPRITES
# GUN AND BULLETS
# NPCS
# GAMEPLAY: usar switches para abrir y cerrar puertas y que los enemigos toquen sensores para pasar de nivel
# TODO: FIX enemies stuck in corners
# CONTROLS: WASD movimiento, Z jump X fire DOWN interactuar
# Implementacion: personaje en area interactiva, se asoma [!] sobre objeto, luego up para interactuar
# Objetos y Armas: Solo un objeto equipado a la vez, se cambia con NEXT/PREV object, se dispara/usa con X
# Objetos: Player usa la funcion fire() del [current_object], este objecto se encarga del resto.
# Items: 8x8 Item tile(objeto en mundo/icono inventario) 4x4/4x8 Item in player overlay, opcional: 8x8/16x8 Item effect al usarlo

# Mission 1: Conseguir el pico y minar hacia la siguiente zona, puzzles con mecanica de minado y switches, crear carritos
# TODO: Programar interaccion del pico, animacion y area2d del pico como area de items genericos
# IDEA: pixel per pixel destruccion/corrupcion y borrar tile al 20% de pixels restantes
