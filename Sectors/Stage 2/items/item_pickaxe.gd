extends Node2D
class_name Item

onready var fire_entity = load("res://Sectors/Stage 2/entities/pickaxe_small.tscn")
func _on_dialog_event_listener_dialog_ended():
	visible = false
	LevelLoader.Inventory.add_item(self)
	# queue_free()

func item_fire():
	LevelLoader.Player.particles.restart()
	# Create sprite? create collision
	# Check tile collision?
	var pickaxe_entity = fire_entity.instance()
	# Add it to level and process the logic on 
	LevelLoader.current_level.add_child(pickaxe_entity)
	# LevelLoader.Player.pivot.add_child(pickaxe_copy)

func item_equip():
	pass

func item_unequip():
	pass

# TODO: fix pico min en el coso de colision y posicionar bien el area2d, detectar tilessolamente y coordenadas
