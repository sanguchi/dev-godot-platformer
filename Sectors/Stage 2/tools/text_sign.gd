extends Node2D

export (int) var sign_sprite_index = 0
export (String, MULTILINE) var dialog_text = """THIS IS text_sign DEFAULT DIALOG TEXT
PLEASE FILL THE Dialog Text TEXTBOX FOR THIS ENTITY"""

# Called when the node enters the scene tree for the first time.
func _ready():
	if(sign_sprite_index < $Sprite.hframes):
		$Sprite.frame = sign_sprite_index
	if(dialog_text):
		$dialog_event_listener.dialog_text = dialog_text
		


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
