extends Node2D
export (bool) var fixed_to_area2d = false
export (int) var camera_margin = 0
onready var cam = $Camera2D
onready var area = $Area2D
onready var collision_shape = $Area2D/CollisionShape2D


func _ready():
	var shape: RectangleShape2D = collision_shape.shape
	if(fixed_to_area2d):
		
		cam.limit_top = area.global_position.y - shape.extents.y
		cam.limit_bottom = area.global_position.y + shape.extents.y
		cam.limit_left = area.global_position.x - shape.extents.x
		cam.limit_right = area.global_position.x + shape.extents.x
	if(camera_margin):
		shape.extents.x += camera_margin
		shape.extents.y += camera_margin


func _on_Area2D_body_entered(body):
	if(body == LevelLoader.Player):
		# print("FOLLOW CAMERA ", body)
		self.remove_child(cam)
		body.add_child(cam)
		cam.current = true


func _on_Area2D_body_exited(body):
	if(body == LevelLoader.Player):
		# print("FOLLOW CAMERA EXIT ", body)
		body.remove_child(cam)
		call_deferred("add_child", cam)
		cam.current = false
