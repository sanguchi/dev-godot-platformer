extends Node2D


export (NodePath) var target_node
export (String) var toggle_text = ""

# Called when the node enters the scene tree for the first time.
func _ready():
	if(target_node):
		if(not get_node(target_node).has_method("action")):
			push_error("the linked node[%s] has no method action()" % target_node)
	else:
		push_error("%s linked node not set, switch will not work" % self)


func _on_action_label_action_label():
	$Particles2D.emitting = true
	$Sprite.frame = 0 if $Sprite.frame else 1
	if(target_node):
		var target_instance = get_node(target_node)
		if(target_instance.has_method("action")):
			target_instance.action()
			if(toggle_text):
				LevelLoader.load_dialog(toggle_text.split('\n'))
		else:
			push_error("the linked node[%s] has no method action()" % target_node)
	else:
		push_warning("%s linked node not set, switch will not work" % self)
