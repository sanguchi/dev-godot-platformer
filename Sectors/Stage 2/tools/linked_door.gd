extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (NodePath) var destination_door

# Called when the node enters the scene tree for the first time.
func _ready():
	if(not destination_door):
		push_error("%s linked door not set" % self)


func _on_action_label_action_label():
	$AnimationPlayer.play("action")
	if(destination_door):
		var door_instance = get_node(destination_door)
		LevelLoader.Player.position = door_instance.global_position
		door_instance.get_node("AnimationPlayer").play("action")
	else:
		push_error("%s linked door not set, player will not move" % self)
