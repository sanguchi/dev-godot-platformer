extends Node2D

export (bool) var is_open = false
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	toggle()

func action():
	toggle()

func toggle():
	if(is_open):
		$AnimationPlayer.play("open")
	else:
		$AnimationPlayer.play("close")
	is_open = not is_open
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
