extends Node2D

# deberia estar ligado a una puerta / area2d y mostrarse una sola vez
# sprite de fondo opaco, lineas de borde y texto de zona
# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export (String) var text_line = "Zone Label\nText"


# Called when the node enters the scene tree for the first time.
func _ready():
	$Control/Label.text = text_line
	$Control/TextureRect.rect_size = $Control/Label.rect_size
	print($Control/Label.get_rect())
	print($Control/TextureRect.get_rect()) 
	# $Control/TextureRect.set_size($Control/Label.rect_size)
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func _on_Area2D_area_entered(area):
	pass # Replace with function body.


func _on_Area2D_area_exited(area):
	pass # Replace with function body.
