extends Node2D

# Item List is an Array of INSTANCES, levels must remove child and attach to this
var item_list: Array = []
var current_item = null
var current_index = 0
var player_item_overlay = Sprite.new()
# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if(not item_list.empty()):
		process_keys()
	display_hud()
	show_overlay()
	
func process_keys():	
	if(Input.is_action_just_released("controls_next")):
		current_index += 1
		current_index = current_index % item_list.size()
	if(Input.is_action_just_released("controls_prev")):
		current_index -= 1
		if(current_index < 0):
			current_index = item_list.size() - 1
	# This checks prevents equip the item twice
	if(!current_item == item_list[current_index]):
		# if()
		current_item = item_list[current_index]
		
	if(Input.is_action_just_pressed("controls_fire")):
		if((current_item as Item).has_method("item_fire")):
			current_item.item_fire()

func display_hud():
	if(not item_list):
		visible = false
	else:
		visible = true
		if(item_list.size() <= 1):
			$label_left.visible = false
			$label_right.visible = false
		else:
			$label_left.visible = true
			$label_right.visible = true
		# Sprite is for display on inventory widget, SpriteSmall is for in world use/overlay
		$Sprite.texture = current_item.get_node("Sprite").texture
		$Sprite.hframes = current_item.get_node("Sprite").hframes
		player_item_overlay.texture = current_item.get_node("SpriteSmall").texture
		player_item_overlay.hframes = current_item.get_node("SpriteSmall").hframes
		# player_item_overlay.frame = 1


func show_overlay():
	var overlay: Sprite = LevelLoader.Player.get_node("PivotCenter/Pivot/SpriteOverlay")
	if(item_list.empty()):
		overlay.visible = false
	else:
		overlay.visible = true


func add_item(item_instance: Item):
	item_list.append(item_instance)
	item_instance.get_parent().remove_child(item_instance)
	$items.add_child(item_instance)
	if(item_list.size() == 1): # Picking a item with empty inventory
		current_item = item_instance
		current_index = 0  # Por las dudas no vaya a ser que en un futuro cambie esto y no me acuerde
		
		
		
