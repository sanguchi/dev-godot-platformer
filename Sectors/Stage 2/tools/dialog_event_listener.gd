extends Node2D

signal dialog_ended

export(String, MULTILINE) var dialog_text = ""

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	if(not dialog_text):
		push_error("%s dialog_text is not set, no dialog will trigger." % self)




func _on_action_label_action_label():
	assert(not dialog_text.empty(), "%s dialog_text is not set, no dialog will trigger" % self)
	if(dialog_text):
		LevelLoader.load_dialog(dialog_text.split("\n"))
		# By default the dialog is null before the call, trying to interact with the dialog will
		# raise a null exception
		if(LevelLoader.Dialog.is_connected("dialog_finished", self, "on_dialog_finished")):
			LevelLoader.Dialog.disconnect("dialog_finished", self, "on_dialog_finished")
		var _error = LevelLoader.Dialog.connect("dialog_finished", self, "on_dialog_finished")
		

func on_dialog_finished():
	if(LevelLoader.Dialog.is_connected("dialog_finished", self, "on_dialog_finished")):
		LevelLoader.Dialog.disconnect("dialog_finished", self, "on_dialog_finished")
	print("Dialog listener: Dialog ended")
	emit_signal("dialog_ended")
