extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
signal door_action(orig_door, dest_door)

# Called when the node enters the scene tree for the first time.
func _ready():
	# Bind array [from_door, to_door]
	var _error = $door1/ActionHint.connect("action", self, "action_door", [$door1, $door2])
	_error = $door2/ActionHint.connect("action", self, "action_door", [$door2, $door1])
	_error = $door1/Area2D.connect("area_entered", self, "area_enter", [$door1])
	_error = $door2/Area2D.connect("area_entered", self, "area_enter", [$door2])
	_error = $door1/Area2D.connect("area_exited", self, "area_exit", [$door1])
	_error = $door2/Area2D.connect("area_exited", self, "area_exit", [$door2])
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func area_enter(_area, door):
	# print("Area enter door", door)
	door.get_node("ActionHint").visible = true

func area_exit(_area, door):
	door.get_node("ActionHint").visible = false

func action_door(from_door, dest_door):
	from_door.get_node("AnimationPlayer").play("action")
	dest_door.get_node("AnimationPlayer").play("action")
	emit_signal("door_action", from_door, dest_door)


