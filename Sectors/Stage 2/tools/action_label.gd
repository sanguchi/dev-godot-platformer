extends Node2D


signal action_label

# Called when the node enters the scene tree for the first time.
func _ready():
		var _c = $Area2D.connect("area_entered", self, "on_area_entered")
		_c = $Area2D.connect("area_exited", self, "on_area_exited")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if Input.is_action_just_pressed("controls_down") and $Control.visible and not get_tree().paused:
		# print("action_label signal emmited", get_tree().paused)
		emit_signal("action_label")

func on_area_entered(_area):
	$Control.visible = true

func on_area_exited(_area):
	$Control.visible = false
