extends Node2D

export (int) var sign_sprite_index = 0
export (String, MULTILINE) var dialog_text = """Zone 7
Center Region"""

# Called when the node enters the scene tree for the first time.
func _ready():
	if(sign_sprite_index < $Sprite.hframes):
		$Sprite.frame = sign_sprite_index
	if(dialog_text):
		$dialog_event_listener.dialog_text = dialog_text
	$Control/Label.text = dialog_text

# TODO: adjust borders, gray background, custom area enter and 
# assign / unassign label children to LevelLoader	
# calculate size and center based on text lenght


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
