extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export(String) var target_scene
# Called when the node enters the scene tree for the first time.
func _ready():
	var _error = $ActionHint.connect("action", self, "_on_ActionHint_action")
	

func _on_ActionHint_action():
	# print("Action signal received")
	# print(target_scene)
	LevelLoader.load_sector("res://Sectors/Stage 2/level.tscn")
	# LevelLoader.Player.position = get_node(target).position
	
	


func _on_Area2D_area_entered(_area):
	$ActionHint.visible = true


func _on_Area2D_area_exited(_area):
	$ActionHint.visible = false
