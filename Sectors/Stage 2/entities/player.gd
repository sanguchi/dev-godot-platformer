extends KinematicBody2D

const TARGET_FPS = 60
const ACCELERATION = 14
const MAX_SPEED = 70
const FRICTION = 16
const AIR_RESISTANCE = 4
const GRAVITY = 8
const JUMP_FORCE = 140

var motion = Vector2.ZERO

onready var sprite = $Sprite
onready var animationPlayer = $AnimationPlayer

var dialog = load("res://Tools/Dialog.tscn").instance()

func _process(delta):
	if(Input.is_action_just_pressed("ui_down") and is_on_floor()):
		$Label/Timer.start(1)
		$Label.visible = true
	
func _physics_process(delta):
	var x_input = Input.get_action_strength("controls_right") - Input.get_action_strength("controls_left")
	
	if x_input != 0:
		animationPlayer.play("run")
		motion.x += x_input * ACCELERATION * delta * TARGET_FPS
		motion.x = clamp(motion.x, -MAX_SPEED, MAX_SPEED)
		sprite.flip_h = x_input < 0
		# -2 left 6 right
		# $VisibilityNotifier2D.position.x = -2 if sprite.flip_h else 6
	else:
		animationPlayer.play("idle")
	
	motion.y += GRAVITY * delta * TARGET_FPS
	
	if is_on_floor():
		if x_input == 0:
			motion.x = lerp(motion.x, 0, FRICTION * delta)
			
		if Input.is_action_just_pressed("controls_jump"):
			print("XDD JUMP")
			motion.y = -JUMP_FORCE
	else:
		animationPlayer.play("jump")
		
		if Input.is_action_just_released("controls_jump") and motion.y < -JUMP_FORCE / 2.0:
			motion.y = -JUMP_FORCE / 2.0
		
		if x_input == 0:
			motion.x = lerp(motion.x, 0, AIR_RESISTANCE * delta)
	
	# motion.y = lerp(0, 70, motion.y)
	motion = move_and_slide(motion, Vector2.UP, false, 4, PI/4, false)
	# Push objects
	for index in get_slide_count():
		var collision = get_slide_collision(index)
		if collision.collider.get_class() == ("RigidBody2D"):
			collision.collider.apply_central_impulse(-collision.normal * 4)
		# print(collision.collider.get_class())


func _on_Timer_timeout():
	$Label.visible = false
