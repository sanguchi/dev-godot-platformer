extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var direction = Vector2.RIGHT
var TARGET_FPS = 60
var MAX_SPEED = 100
var timer = Timer.new()
export (int) var velocity = 2
# Called when the node enters the scene tree for the first time.
func _ready():
	direction = LevelLoader.Player.direction_facing
	add_child(timer)
	timer.connect("timeout", self, "timer_finish")
	# print("pickaxe bullet created")
	timer.start(0.1)
	position = LevelLoader.Player.global_position
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _physics_process(delta):
	position += direction * delta * velocity * TARGET_FPS
	# position.x = clamp(position.x, -MAX_SPEED, MAX_SPEED)
	# position.
	
	
func timer_finish():
	# print("pickaxe bullet deleted")
	queue_free()


func _on_Area2D_body_entered(body: TileMap):
	# print(body as TileMapDestroyable) # Replace with function body.
	if(body as TileMapDestroyable):
		var global_v = global_position
		var local_v = body.to_local(global_position)
		var tile_v = body.world_to_map(local_v)
		print("Destroying tile ", global_v, local_v, tile_v)
		body.set_cellv(tile_v + direction, -1)
	queue_free()
