extends Node2D

# Sectors do:
# Manage entities
# Manage furniture and objects
# Provide a player starting point for LevelLoader
# Pauses and executes dialog
# Cutscenes?
# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	# print("[FirstSector]: Loaded, calling Entry Dialog")
	_on_Timer_timeout()
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Timer_timeout():
	# print("Timer finished")
	
#	LevelLoader.load_dialog([
#		"Hello there, Press Space/X to continue...", 
#		"You can move with WASD/Arrows.",
#		"Use Space/X to interact.", 
#		"Press Space/X to close this dialog."])
	pass
		


func _on_Switch_switch_changed():
	if $Switch.status:
		$Door/AnimationPlayer.play("open")
	else:
		$Door/AnimationPlayer.play("close")
		

