extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
export(Array, String) var textlines = ["DEFAULT TEXT, PRESS X", "THANKS FOR PLAYING"]
export(String) var full_text = "DEFAULT TEXT, EVERY NEWLINE...\n...\n...is another dialog screen"
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2DAction_body_entered(body):
	print("BODY ENTERED ", body)


func _on_Area2DAction_body_exited(body):
	print("BODY EXITED ", body)


func _on_Area2DAction_area_entered(_area):
	# print("AREA ENTERED ", area)
	$ActionHint.visible = true
	# print(area.get_parent())


func _on_Area2DAction_area_exited(_area):
	$ActionHint.visible = false
	# print("AREA EXITED ", area)


func _on_ActionHint_action():
	# print("Action signal received")
	if(textlines):
		LevelLoader.load_dialog(textlines)
	elif(full_text):
		LevelLoader.load_dialog(full_text.split("\n"))
	else:
		push_warning("%s no text set on textsign" % self)
		LevelLoader.load_dialog(["DEFAULT TEXT, PRESS X", "THANKS FOR PLAYING"])
	# LevelLoader.load_dialog(textlines)
	
