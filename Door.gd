extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
# export()

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Area2D_area_entered(area):
	print(area)
	$ActionHint.visible = true


func _on_Area2D_area_exited(area):
	print(area)
	$ActionHint.visible = false


func _on_ActionHint_action():
	print("Action")
