extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
# onready var room_width = $Camera2D.transform.
# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_VisibilityNotifier2D_viewport_exited(viewport):
	var player_pos = $Player.position
	var x = floor(player_pos.x / (viewport.size.x / 4))
	var y = floor(player_pos.y / (viewport.size.y / 4))
	var grid_position = Vector2(x, y)
	print(grid_position)
	$Camera2D.position = grid_position * Vector2(160, 120)

	
