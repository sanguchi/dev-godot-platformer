extends Node2D


signal action

# Called when the node enters the scene tree for the first time.
func _ready():
		var _c = $Area2D.connect("area_entered", self, "on_area_entered")
		_c = $Area2D.connect("area_exited", self, "on_area_exited")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if Input.is_action_just_pressed("controls_down") and visible:
		# print("Action Signal Emmited")
		emit_signal("action")

func on_area_entered(_area):
	visible = true

func on_area_exited(_area):
	visible = false
